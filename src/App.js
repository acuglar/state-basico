import './App.css';
import React from "react";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showDiv: true,
      showH1: true,
      showH2: true,
      isH3: true,
      isBold: true
    }
  }

  hideButton = () => {
    this.setState({ showDiv: false });
  }

  showButton = () => {
    this.setState({ showDiv: true });
  }

  hideButtonH1 = () => {
    this.setState({ showH1: false });
  }

  showButtonH1 = () => {
    this.setState({ showH1: true });
  }

  hideButtonH2 = () => {
    this.setState({ showH2: false });
  }

  showButtonH2 = () => {
    this.setState({ showH2: true });
  }

  isButtonH3 = () => {
    const { isH3 } = this.state;
    this.setState({ isH3: !isH3 });
  }

  isButtonBold = () => {
    console.log("bold")
    const { isBold } = this.state;
    this.setState({ isBold: !isBold });
  }

  render() {
    return (
      <div>
        <div>
          <button onClick={this.hideButton}>Hide div</button>
          <button onClick={this.showButton}>Show div</button>
          {this.state.showDiv && <div>Olá, div!</div>}  {/*id. this.state.showDiv ? <div>Olá React</div> : null */}
        </div>

        <div>
          <button onClick={this.hideButtonH1}>Hide h1</button>
          <button onClick={this.showButtonH1}>Show h1</button>
          {this.state.showH1 && <h1>Olá, h1!</h1>}
        </div>

        <div>
          <button onClick={this.hideButtonH2}>Hide h2</button>
          <button onClick={this.showButtonH2}>Show h2</button>
          {this.state.showH2 && <h2>Olá, h2!</h2>}
        </div>

        <div>
          <button onClick={this.isButtonH3}>Show/Hide h3</button>
          {this.state.isH3 && <h3>Olá, h3!</h3>}
        </div>

        <div>
          <button onClick={this.isButtonBold}>Show/Hide bold</button>
          {this.state.isBold ? <div><b>Olá com negrito</b></div> : <div>Olá sem negrito</div>}
        </div>

      </div >
    )
  }
}


export default App;
